import db_manager as db
import numpy as np


# Проверить пришло ли сообщение из группового чата
# Check if a message came from a group chat
def check_group_chat(bot, message):
    print("Проверяем групповой ли это чат")
    if message.chat.id < 0:
        return True
    else:
        bot.reply_to(message, "Ошибка!\nЭта команда только для групповых чатов")
        return False


# Проверить является ли отправитель сообщения админом
# Check if the sender of the message is an admin
def check_admin_access(bot, message):
    if check_group_chat(bot, message):
        print("Проверяем статус админа отправителя")
        user_id = message.from_user.id
        admins = bot.get_chat_administrators(message.chat.id)
        for admin in admins:
            if user_id == admin.user.id:
                return True
        bot.reply_to(message, "Ошибка!\nДоступ к этой команде имеют только администраторы чата")
        return False


# Добавить новых пользователей в игру (только для админов)
# Add new users to the game (only for admins)
def add_new_user_comand(bot, message, username=None):
    print("Добавление нового пользователя в чат с id=", message.chat.id)
    # Попытка добавить нового пользователя при его попадании в чат
    # Attempt to add a new user when he enters the chat
    if username:
        username = username.lower()
        flag = db.add_new_user(message.chat.id, username)
        if flag:
            bot.reply_to(message, f"{username} добавлен в базу данных")
            print("Успешно добавлен новый пользователь")
        else:
            bot.reply_to(message, f"Пользователь {username} уже есть в базе")
            print(username, "уже был в базе")
        return

    # Если вызвана команда /add_new_user
    # If the command /add_new_user is called
    users_list = message.text.split(",")
    for username in users_list:
        username = username.strip()
        username = username.lower()
        if username == "" or " " in username:
            bot.reply_to(message, "Ошибка!\nКак минимум один из пользователей указан неверно. ")
            print("Не смог распознать пользователей", username)
            return
        # добавляем @ если её нет
        if username[0] != "@":
            username = "@" + username
        flag = db.add_new_user(message.chat.id, username)
        if flag:
            bot.reply_to(message, f"{username} добавлен в базу данных")
            print("Успешно добавлен новый пользователь")
        else:
            bot.reply_to(message, f"Ошибка!\nПользователь {username} уже есть в базе")
            print(username, "уже был в базе")


# удалить пользователей из игры (для админов)
def delete_user_comand(bot, message, user_name=None):
    if user_name is not None:
        user_name = user_name.lower()
        print("Пробуем удалить", user_name, "из чата")
        user_id = db.get_users(message.chat.id, username=user_name)
        success = db.delete_user(user_id)
        if success:
            bot.send_message(message.chat.id, text=f"{user_name} удалён из базы")
        else:
            bot.send_message(message.chat.id, text=f"Кажется {user_name} не было в списке, по крайней мере я не нашёл.")
        return

    chat_id = message.chat.id
    example = "Пример правильной команды\n/delete_user user1, @user2, user3"
    users_list = message.text.split(",")
    if len(users_list) == 0:
        bot.reply_to(message, "Ошибка!\nНе смог распознать пользователей. " + example)
        print("Не смог распознать пользователей")
        return False
    for username in users_list:
        username = username.strip()
        username = username.lower()
        if username == "" or " " in username:
            bot.reply_to(message, "Ошибка!\nКак минимум один из пользователей указан неверно. " + example)
            return False
        if username[0] != "@":
            username = "@" + username
        user_id = db.get_users(message.chat.id, username=username)
        flag = db.delete_user(user_id)
        if flag:
            bot.reply_to(message, f"{username} удалён из базы данных")

        else:
            bot.reply_to(message, f"Ошибка!\nПользователя {username} и так не было в базе данных")


def delete_quest_comand(bot, message):
    chat_id = message.chat.id
    print("Получил запрос на удаление задания из чата", chat_id)
    quests_id_in_chat = db.get_quests_id(chat_id)
    example = "Пример правильной команды\n/delete_quest 1, 2, 3"
    quests_list = message.text.split(",")
    if len(quests_list) == 0:
        print("Не смог распознать id заданий")
        bot.reply_to(message, "Ошибка!\nНе смог распознать id заданий. " + example)
        return False
    print("Список id=", quests_list)
    for quest_id in quests_list:
        quest_id = quest_id.strip()
        try:
            quest_id = int(quest_id)
        except ValueError:
            bot.reply_to(message, f"Ошибка! id должен быть числом, а вы отправили '{quest_id}'\n" + example)
            return False
        if quest_id not in quests_id_in_chat:
            bot.reply_to(message, "Ошибка! В вашем чате нет задания с таким id")
            return False
        success = db.delete_quest(quest_id)
        if success:
            bot.reply_to(message, f"Задание с id={quest_id} успешно удалено")
            return True
        else:
            bot.reply_to(message, f"Ошибка! Задание с id={quest_id} не получилось удалить")
            return False


def get_users_comand(bot, message):
    users_list = ""
    users_list_in_db = db.get_users(message.chat.id)
    for user in users_list_in_db:
        users_list += user + ", "
    if len(users_list) > 2:
        users_list = users_list[:-2]
        bot.reply_to(message, f"Список игроков этого чата:\n{users_list}")
    else:
        bot.reply_to(message, f"А игроков нет :(\nЧтобы добавить воспользуйтесь командой "
                              f"/add_new_user или введите /help для просмотра всех команд")


# отправить список доступных заданий для этого чата
def get_quests_comand(bot, message):
    quests_id = db.get_quests_id(message.chat.id)
    if len(quests_id) > 0:
        quests = db.get_quests(quests_id)
        bot.reply_to(message, f"Список заданий этого чата:")
        for quest in quests:
            quest_msg = f"id={quest}\n{quests[quest][0]}"
            if quests[quest][1] is None:
                bot.send_message(message.chat.id, quest_msg)
            else:
                bot.send_photo(message.chat.id, quests[quest][1], caption=quest_msg)
    else:
        bot.reply_to(message, "А заданий нет :(\nЧтобы добавить воспользуйтесь командой "
                              "/add_new_quest или введите /help для просмотра всех команд")


def add_new_quest_comand(message):
    print("Получили запрос на добавление нового квеста")
    if message.photo is None:
        text = message.text
        db.add_new_quest(message.chat.id, text)
        print("Текст без картинки\n", text)
    else:
        text = message.caption
        file_id = message.photo[0].file_id
        print("Получили картинку, text=", text)
        print(file_id)
        db.add_new_quest(message.chat.id, text, file_id)


def make_quests_comand(bot, message):
    print("Начинаем генерацию заданий для чата", )
    chat_id = message.chat.id
    number_of_quests = db.get_number_of_quests(chat_id)

    if number_of_quests < 1:
        number_of_quests = 1
    users = db.get_users(chat_id)
    quests_id = db.get_quests_id(chat_id)

    if number_of_quests > len(users) or number_of_quests > len(quests_id):
        number_of_quests = len(users) if len(users) < len(quests_id) else len(quests_id)  # выбираем меньшее

    rand_members = np.random.choice(users, number_of_quests, replace=False)
    rand_quests_id = np.random.choice(quests_id, number_of_quests, replace=False)

    rand_quests = db.get_quests(rand_quests_id)
    result = {}
    for i in range(0, number_of_quests):
        member = rand_members[i]
        quest = rand_quests[i][0]
        image = rand_quests[i][1]
        result[i] = [member, quest, image]
    print(result)

    # quests = make_quests(chat_id)
    # if quests:
    #     for msg in quests.values():
    #         member = msg[0]
    #         quest_text = msg[1]
    #         quest = f"{member}, {quest_text}"
    #         photo = msg[2]
    #         if photo == "":
    #             bot.send_message(chat_id, quest)
    #         else:
    #             bot.send_photo(chat_id, photo, caption=quest)
    # else:
    #     error_msg = "Что-то пошло не так. Я не смог сформировать задания."
    #     bot.send_message(chat_id, error_msg)

    return result