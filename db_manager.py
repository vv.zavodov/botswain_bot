from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


# Сущность чата
# Entity of the chat
class Chat(Base):
    __tablename__ = 'Chats'
    chat_id = Column(String, primary_key=True)
    number_of_quests = Column(Integer)

    def __init__(self, chat_id, number_of_quests=3):
        self.chat_id = chat_id
        if number_of_quests > 0:
            self.number_of_quests = number_of_quests
        else:
            self.number_of_quests = 1

    def __repr__(self):
        return "<Chat(%s)>" % self.chat_id


# Сущность пользователя
# Entity of the user
class User(Base):
    __tablename__ = 'Users'
    user_id = Column(Integer, primary_key=True)
    chat_id = Column(String, ForeignKey(Chat.chat_id))
    username = Column(String)

    def __init__(self, chat_id, username):
        self.chat_id = chat_id

        if username[0] == "@":
            self.username = username
        else:
            self.username = "@%s" % username

    def __repr__(self):
        return "<User(%s, id=%s, chat_id=%s)>" % (self.username, self.user_id, self.chat_id)


# Сущность задания
# Entity of the quest
class Quest(Base):
    __tablename__ = 'Quests'
    quest_id = Column(Integer, primary_key=True)
    chat_id = Column(String, ForeignKey(Chat.chat_id))
    text = Column(String)
    image = Column(String)

    def __init__(self, chat_id, text, image=None):
        self.chat_id = chat_id
        self.text = text
        self.image = image

    def __repr__(self):
        return "<Quest(id=%s, quests_list_id=%s\ntext\n%s\nimage=%s )>" % (
            self.quest_id, self.chat_id, self.text, self.image)


# Создание файла бд
# Creating a db file
engine = create_engine('sqlite:///bot_data.db', echo=False)
Base.metadata.create_all(engine)


# Создание сессии
# Session creation
def make_session():
    engine = create_engine('sqlite:///bot_data.db', echo=False)
    Session = sessionmaker(bind=engine)
    return Session()


# Функция для отладки. Создаёт тестовую версию базы данных
# Function for debugging. Creates a test version of the database
def create_db():
    chat_id = "-123456789"
    quest_text = "выполни зарядку!\nЗдоровье это важно."
    quest_text2 = "Скажи А!\nБла бла бла."
    image = "https://lim-english.com/uploads/images/all/Sharon%2C%2520Jon%2C%2520Lou%2520and%2520Dan(1).jpg"

    new_chat = Chat(chat_id)
    new_user = User(chat_id, "Velvazar")
    new_quest1 = Quest(chat_id, quest_text)
    new_quest2 = Quest(chat_id, quest_text2, image)

    session = make_session()
    session.add(new_chat)
    session.add(new_user)
    session.add(new_quest1)
    session.add(new_quest2)
    session.commit()


# Получить список чатов
# Get chat list from database
def get_chats():
    print("Получаем список чатов")
    chats_list = []
    session = make_session()
    chats_in_db = session.query(Chat.chat_id).all()
    for chat in chats_in_db:
        chats_list.append(chat[0])
    print(chats_list, "\n")
    return chats_list


# Добавить в бд новый чат
# Add a new chat to the database
def add_new_chat(chat_id):
    print("Получили запрос на добавление чата с id", chat_id)
    session = make_session()
    chats_list = get_chats()
    if str(chat_id) in chats_list:
        print("Такой чат уже есть в базе")
        return False
    else:

        new_chat = Chat(chat_id)
        session.add(new_chat)
    print("Добавили. Теперь создадим базовые квесты")
    #
    # здесь ещё нужно дописать создание стандартных квестов
    #
    print("Готово!")
    session.commit()
    session.close()
    print("Успешно добавлен новый чат!", chat_id, "\n")
    return True


# Добавить в бд нового пользователя
# Add a new user to the database
def add_new_user(chat_id, username):
    session = make_session()
    users = get_users(chat_id)

    print("Уже есть в базе", users)
    print("Добавляем", username)
    print("chat_id", chat_id)
    if username in users:
        return False
    else:
        new_user = User(chat_id, username)
        session.add(new_user)
        session.commit()
        session.close()
        return True


# Добавить новый квест
# Add a new quest to the database
def add_new_quest(chat_id, quest_text, image=None):
    session = make_session()
    new_quest = Quest(chat_id, quest_text, image)
    session.add(new_quest)
    session.commit()
    session.close()


# Удалить чат и всё, что с ним связано
# Delete chat and everything connected with it
def delete_chat(chat_id):
    print("Получили запрос на удаление чата с id", chat_id)
    session = make_session()
    chats_list = get_chats()
    if str(chat_id) not in chats_list:
        print("Такого чата нет в базе")
        return False
    print("Он есть в базе. Приступаем к удалению")
    users = get_users(chat_id)
    for user in users:
        delete_user(user)
    print("Всех удалили!")
    quests = get_quests_id(chat_id)
    print("Квесты этого чата", quests)
    for quest in quests:
        delete_quest(quest)
    print("Всё удалили!")
    success = session.query(Chat).filter_by(chat_id=chat_id).delete()
    session.commit()
    if success:
        print("Удалили сам чат. Готово!\n")
    return success


# Удалить пользователя из чата
# Remove user from chat
def delete_user(user_id):
    print("Получили запрос на удаление пользователя с id", user_id)
    session = make_session()
    success = session.query(User).filter_by(user_id=user_id).delete()
    session.commit()
    if success:
        print("Удалили")
    else:
        print("Не нашли такого пользователя")
    return success


# Удалить квест из чата
# Remove quest from chat
def delete_quest(quest_id):
    print("Получили запрос на удаление квеста с id", quest_id)
    session = make_session()
    success = session.query(Quest).filter_by(quest_id=quest_id).delete()
    session.commit()
    if success:
        print("Удалили")
    else:
        print("Не нашли такой квест")
    return success


# Возвращает список пользователей чата или id одного пользователя в чате
# Returns a list of chat users or the id of one user in a chat
def get_users(chat_id, username=None):
    session = make_session()
    if username:
        print("Получили запрос на id", username)
        user_id = session.query(User.user_id).filter_by(chat_id=chat_id, username=username).first()
        if user_id:
            print("user_id:", user_id)
            return user_id[0] if user_id else False
    else:
        print("Получили запрос на пользователей чата с id =", chat_id)
        users_db = session.query(User.username).filter_by(chat_id=chat_id).all()
        users = []
        for quest in users_db:
            users.append(quest[0])
        print("users:", users)
        return users


# Получить список id всех квестов по id чата
# Get a list of id of all quests by chat id
def get_quests_id(chat_id):
    print("Получили запрос на квесты чата с id =", chat_id)
    session = make_session()
    quests_db = session.query(Quest.quest_id).filter_by(chat_id=chat_id).all()
    quests = []
    for quest in quests_db:
        quests.append(quest[0])
    print("quests_id:", quests)
    return quests


# Получить данные квестов по их id
# Get quest data by their id
def get_quests(id_list):
    print("Получили запрос на данные квестов с id", id_list)
    session = make_session()
    quests = {}
    for i in range(0, len(id_list)):
        q_id = int(id_list[i])
        quest_text = session.query(Quest.text).filter_by(quest_id=q_id).first()
        quest_image = session.query(Quest.image).filter_by(quest_id=q_id).first()
        if quest_text is not None:
            quest_text = quest_text[0]
        if quest_text is not None:
            quest_image = quest_image[0]
        quests[q_id] = [quest_text, quest_image]
    print("quests:", quests)
    return quests


# Получить количество заданий, которые надо выводить в этом чате
# Get the number of quests to be displayed in this chat
def get_number_of_quests(chat_id):
    print("получили запрос количество заданий, которые надо выводить в чате с id", chat_id)
    session = make_session()
    number_of_quests = session.query(Chat.number_of_quests).filter_by(chat_id=chat_id).first()
    if number_of_quests is None:
        print("Не нашли")
        return False
    else:
        number_of_quests = int(number_of_quests[0])
        print("Это число равно", number_of_quests)
        return number_of_quests


# Изменить количество заданий, которые надо выводить в этом чате
# Update the number of quests to be displayed in this chat
def update_number_of_quests(chat_id, new_number_of_quests):
    print("Получили запрос на изменение количества заданий, которые надо выводить в чате с id", chat_id)
    print("Новое значение:", new_number_of_quests)
    session = make_session()
    try:
        session.query(Chat).filter(Chat.chat_id == chat_id).update({Chat.number_of_quests: new_number_of_quests},
                                                                   synchronize_session=False)
        session.commit()
    except SQLAlchemyError:
        print("Не получилось")
        session.rollback()
        return False
    print("Изменили!")
    return True

# # get_users(-1001248913333)
# us = get_users(-1001248913333, "@tenebrial")
# delete_user(23)
# # get_users(-1001248913333)
# add_new_user(-1001248913333, "@tenebrial")
# print("Добавили")
# get_users(-1001248913333)
# # make_quests("-123456789")
# add_new_chat(123)
# delete_chat(123)

# get_number_of_quests(-1001248913333)
# update_number_of_quests(-1001248913333, 3)
# get_number_of_quests(-1001248913333)
# users_list = str(get_users(-1001248913333))
# print(users_list)

# quests_id = get_quests_id(-123456789)
#
# quests = get_quests(quests_id)

# aaa = "asfd qwe qwe qwe"
# aa = aaa.index(" ")+1
# print(aa)
# aaa = aaa[aa:]
# print(aaa)
# for quest in quests:
#     print(quest)
#     print(quests[quest][0])
#     print(quests[quest][1])
#
#     quests_msg = f"id={quest}\n + "
