import telebot
import comands as comands
from settings import bot_token

bot = telebot.TeleBot(bot_token, threaded=True)

# help - инф. по командам !
# make_quests - начать игру !
# get_users - список игроков !
# get_quests - список заданий !
# add_new_user - новый игрок !
# add_new_quest - новое задание !
# delete_user - удалить игроков !
# delete_quest - удалить задания !
help_mssage = "Если хочешь со мной поболтать, напиши /hello\n\n" \
              "Если хочешь запустить генерацию заданий напиши /make_quests\n\n" \
              "Чтобы посмотреть список игроков напиши /get_users\n\n" \
              "Чтобы посмотреть список заданий напиши /get_quests\n\n" \
              "Добавить нового игрока можно при помощи команды /add_new_user\n" \
              "После неё нужно через запятую перечислить username новых игроков. Их можно написать и без @, я пойму.\n" \
              "Эта команда доступна только для админов чата.\n" \
              "Пример команды: /add_new_user user1, @user2\n\n" \
              "Чтобы удалить игрока воспользуйся /delete_user. И в этом же сообщении укажи usrename игрока.\n" \
              "Эта команда доступна только для админов чата.\n\n" \
              "Удалить задание: /delete_quest. И в этом же сообщении укажи id задания.\n" \
              "Узнать его можно просмотрев все задания командой /get_quests\n\n" \
              "Чтобы добавить новое задание отправьте команду /add_new_quest\n" \
              "В следующем сообщении отправьте текст задания. По желанию в этом сообщении может быть картинка " \
              "(только одна, телеграмм не позволяет мне отправлять сразу несколько картинок в одном сообщении)"

NEW_help_mssage = "Если хочешь со мной поболтать, напиши /hello\n\n" \
              "Если хочешь запустить генерацию заданий напиши /make_quests\n\n" \
              "Чтобы посмотреть список игроков напиши /get_users\n\n" \
              "Чтобы посмотреть список заданий напиши /get_quests\n\n" \
              "Добавить нового игрока можно при помощи команды /add_new_user\n" \
              "Далее ответом на моё сообщение нужно будет через запятую перечислить username новых игроков. " \
              "Их можно написать и без @, я пойму.\n" \
              "Эта команда доступна только для админов чата.\n\n" \
              "Чтобы удалить игрока воспользуйся /delete_user. И ответом на моё сообщение укажи usrename игрока.\n" \
              "Эта команда доступна только для админов чата.\n\n" \
              "Удалить задание: /delete_quest. И ответом на моё сообщение укажи id задания.\n" \
              "Узнать его можно просмотрев все задания командой /get_quests\n\n" \
              "Чтобы добавить новое задание отправьте команду /add_new_quest\n" \
              "Далее ответом на моё сообщение отправьте текст задания. " \
              "По желанию в этом сообщении может быть картинка, НО ТОЛЬКО ОДНА " \
              "(телеграмм не позволяет мне отправлять сразу несколько картинок в одном сообщении)"


@bot.message_handler(commands=["hello"])
def send_hello_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}):")
    print(message.text, "\n")
    # if message.text == "/hello" or message.text == "/hello@best_boatswain_bot":
    if message.from_user.username == "tenebrial":
        bot.reply_to(message, f"Привет, самый лучший программист!")
    else:
        bot.reply_to(message, f"Привет, {message.from_user.first_name}!")


@bot.message_handler(commands=["help"])
def send_help_message(message):
    bot.reply_to(message, help_mssage)


# @bot.message_handler(commands=["make_quests"])
# def send_make_quests_message(message):
#     access = comands.check_group_chat(bot, message)
#     if access:
#         quest_message(message.chat.id)


@bot.message_handler(commands=["get_users"])
def send_get_users_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.chat_id}:")
    print(message.text, "\n")
    access = comands.check_group_chat(bot, message)
    if access:
        comands.get_users_comand(bot, message)


@bot.message_handler(commands=["get_quests"])
def send_get_quests_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.chat_id}:")
    print(message.text, "\n")
    access = comands.check_group_chat(bot, message)
    if access:
        comands.get_quests_comand(bot, message)


@bot.message_handler(commands=["add_new_user"])
def start_add_new_user_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.chat_id}:")
    print(message.text, "\n")
    access = comands.check_admin_access(bot, message)
    if access:
        bot.reply_to(message, "А теперь отправьте ответом на это сообщение один или несколько (через запятую) "
                              "username игроков, которых хотите добавить.")
        bot.register_next_step_handler(message, send_add_new_user_message)


def send_add_new_user_message(message):
    comands.add_new_user_comand(bot, message)


@bot.message_handler(commands=["add_new_quest"])
def start_add_new_quest_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.id}:")
    print(message.text, "\n")
    access = comands.check_admin_access(bot, message)
    if access:
        bot.reply_to(message, "А теперь отправьте своё задание ответом на это сообщение")
        bot.register_next_step_handler(message, send_add_new_quest_message)


def send_add_new_quest_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.chat_id}:")
    print(message.text, "\n")
    comands.add_new_quest_comand(message)
    bot.reply_to(message, "Готово!")


@bot.message_handler(commands=["delete_user"])
def start_delete_user_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.chat_id}:")
    print(message.text, "\n")
    print("Запрос на удаление пользователей из чата")
    access = comands.check_admin_access(bot, message)
    print("прошли проверку")
    if access:
        bot.reply_to(message, "А теперь отправьте один или несколько(через запятую) "
                              "username игроков, которых хотите удалить ответом на это сообщение.")

        bot.register_next_step_handler(message, send_delete_user_message)


def send_delete_user_message(message):
    comands.delete_user_comand(bot, message)


@bot.message_handler(commands=["delete_quest"])
def start_delete_quest_message(message):
    print(f"Получили текст от {message.from_user.first_name} ({message.from_user.username}) из чата {message.chat.chat_id}:")
    print(message.text, "\n")
    access = comands.check_admin_access(bot, message)
    if access:
        bot.reply_to(message, "А теперь отправьте id задания, которое хотите удалить ответом на это сообщение.")
        bot.register_next_step_handler(message, send_delete_quest_message)


def send_delete_quest_message(message):
    comands.delete_quest_comand(bot, message)


#####################################################################
#####################################################################
#####################################################################
@bot.message_handler(content_types=['new_chat_members'])
def greeting(message):
    new_username = f"@{message.from_user.username}" if message.from_user.username else None
    if not new_username:
        bot.reply_to(message, text="У тебя нет @Username! Ты не можешь играть!\nНо я всё равно рад тебя видеть!")
    else:
        bot.reply_to(message, text=f'Привет, {message.from_user.first_name}!\nБудь как дома :)')
        comands.add_new_user_comand(bot, message, username=new_username)


@bot.message_handler(content_types=['left_chat_member'])
def goodbye(message):
    first_name = message.from_user.first_name
    username = f"@{message.from_user.username}" if message.from_user.username else None
    print(f"Пользователь{first_name} (username={username}) вышел из чата")
    bot.send_message(message.chat.id, text=f"Прощай, {first_name}!")
    if username:
        comands.delete_user_comand(bot, message, user_name=username)
    else:
        bot.send_message(message.chat.id, text=f"{first_name} не был занесён в список, значит и удалять ничего не надо.")


# @bot.message_handler(content_types=['text'])
# def dont_understend(message):
#     bot.reply_to(message, "Я тебя не понимаю. Для просмотра списка команд напиши /help")
#     print("Получили сообщение от", message.from_user.first_name, "\nid=", message.chat.id, "text=", message.text)


def start_command(message):
    bot.send_message(message.chat.id, "Привет!\nЯ супер бот, разработанный Абсолютом, \n"
                                      "чтобы выдавать задания и роли случайным участникам чата.\n\n"
                                      "Для получения списка команд напишите /help")


# @bot.message_handler(commands=["delete_quest"])
# def quest_message(message):
#     bot.send_message(chat_id, "Это пока не работает")
#     comands.make_quests_comand(bot, message)


bot.polling(none_stop=True, interval=5)
